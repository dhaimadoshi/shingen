﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class ArrowControl : MonoBehaviour
    {

        public RectTransform Arrow;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Vector3 pos = RectTransformUtility.WorldToScreenPoint(null, Arrow.anchoredPosition3D);
            // Vector3 pos = Camera.main.WorldToScreenPoint(Arrow.anchoredPosition3D);
            Vector3 dir = Input.mousePosition - pos;

			var size = Mathf.Sqrt(Vector2.SqrMagnitude(dir - pos));
			Arrow.sizeDelta = new Vector2(Arrow.sizeDelta.x, size);
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            Arrow.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
			//Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }
}