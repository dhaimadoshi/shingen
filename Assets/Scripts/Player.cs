﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Shingen;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

public class Player
{
    readonly Deck _deck;
    private SimpleRessourcesCache _ressourcesCache;

    public Player(Deck deck, DeckData deckData, SimpleRessourcesCache ressourcesCache, List<Image> ressourceSymbols)
    {
        _deck = deck;
        _deck.SetDeck(deckData);
        _ressourcesCache = ressourcesCache;
        _ressourcesCache.Initialize(ressourceSymbols, new RessourceList());
    }

    internal void AddCardToDeck(CardData data)
    {
        _deck.Add(data);
    }

    internal Card DrawIndex(int value)
    {
        return _deck.DrawIndex(value);
    }

    public void AddRessources(RessourceValue r)
    {
        _ressourcesCache.AddRessources(r);
    }

    public void DepleteRessources(RessourceList r)
    {
        r.GetList().ForEach(x => _ressourcesCache.DepleteRessources(x));
    }

    public bool HasRessources(RessourceValue r)
    {
        return _ressourcesCache.HasRessources(r);
    }

    public bool HasRessources(RessourceList r)
    {
        bool hasRessources = true;

        r.GetList().ForEach(x => 
        {
            if(!_ressourcesCache.HasRessources(x)) 
            {
                hasRessources = false;
                return;
            }
        });

        return hasRessources;
    }

    internal void AddRessourcesWithConstraint(List<RessourceValue> ressources, List<RessourceValue> constraint)
    {
        var ressourceType = ressources.Join(constraint,
                                        res => res.Type,
                                        constr => constr.Type,
                                        (res, constr) => res.Value <= constr.Value ? res : constr)
                                        .ForEach(x => _ressourcesCache.AddRessources(x));

        // ressources.GetList().Select(x => x.Type)
        // .Join(constraint.GetList().Select(x => x.Type))
    }
}
