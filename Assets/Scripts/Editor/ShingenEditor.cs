﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Sirenix.OdinInspector.Editor;
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using UnityEditor;
    using UnityEngine;

    public class ShingenEditor : OdinMenuEditorWindow
    {
        [MenuItem("DaiGames/Shingen")]
        private static void Open()
        {
            var window = GetWindow<ShingenEditor>();
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 500);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree(true);
            tree.DefaultMenuStyle.IconSize = 28.00f;
            tree.Config.DrawSearchToolbar = true;

            tree.AddAllAssetsAtPath("", "Assets/Resources/Cards", typeof(CardData), true)
                .SortMenuItemsByName();

            tree.AddAllAssetsAtPath("Decks", "Assets/Resources/Decks", typeof(DeckData), true, true)
                .SortMenuItemsByName();

            tree.EnumerateTree().AddIcons<CardData>(x => x.ManaSymbol);

            return tree;
        }

        protected override void OnBeginDrawEditors()
        {
            var selected = this.MenuTree.Selection.FirstOrDefault();
            var toolbarHeight = this.MenuTree.Config.SearchToolbarHeight;

            // Draws a toolbar with the name of the currently selected menu item.
            SirenixEditorGUI.BeginHorizontalToolbar(toolbarHeight);
            {
                if (selected != null)
                {
                    GUILayout.Label(selected.Name);
                }

                if (SirenixEditorGUI.ToolbarButton(new GUIContent("Create Card")))
                {
                    ScriptableObjectCreator.ShowDialog<CardData>("Assets/Resources/Cards", obj =>
                    {
                        obj.Name = obj.name;
                        base.TrySelectMenuItemWithObject(obj); // Selects the newly created item in the editor
                    });
                }

                if (SirenixEditorGUI.ToolbarButton(new GUIContent("Create Deck")))
                {
                    ScriptableObjectCreator.ShowDialog<DeckData>("Assets/Resources/Decks", obj =>
                    {
                        obj.Name = obj.name;
                        base.TrySelectMenuItemWithObject(obj); // Selects the newly created item in the editor
                    });
                }
            }
            SirenixEditorGUI.EndHorizontalToolbar();
        }

    }
}