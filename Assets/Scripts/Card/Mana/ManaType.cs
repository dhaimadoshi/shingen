using System;
using UnityEngine;

namespace Shingen
{
    public enum ManaType
    {
        blue,
        red,
        green,
        black,
        white
    }
}