namespace Shingen
{
    using System;
    using Sirenix.OdinInspector;
    using UnityEngine;

    [Serializable]
    public struct ManaValue : IEquatable<ManaValue>
    {
        [HideInInspector]
        public ManaType Type;

        [Range(0, 10)]
        [LabelWidth(70)]
        [LabelText("$Type")]
        public int Value;

        public ManaValue(ManaType type, int value)
        {
            this.Type = type;
            this.Value = value;
        }

        public ManaValue(ManaType type)
        {
            this.Type = type;
            this.Value = 0;
        }

        public bool Equals(ManaValue other)
        {
            return this.Type == other.Type && this.Value == other.Value;
        }
    }
}