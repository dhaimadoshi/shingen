namespace Shingen
{
    using System;
    using UnityEngine;

    public static class ManaHelper
    {
        public static readonly string black = "Textures/black";
        public static readonly string green = "Textures/green";
        public static readonly string white = "Textures/white";
        public static readonly string red = "Textures/red";
        public static readonly string blue = "Textures/blue";

        public static Sprite GetManaSymbol(ManaType manaType)
            => Resources.Load<Sprite>(GetPath(manaType));
        public static string GetPath(ManaType manaType)
        {
            switch (manaType)
            {
                case ManaType.black:
                    return black;

                case ManaType.blue:
                    return blue;

                case ManaType.white:
                    return white;

                case ManaType.green:
                    return green;

                case ManaType.red:
                    return red;

                default:
                    throw new Exception("card color not handled");
            }
        }
    }
}