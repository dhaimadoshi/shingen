namespace Shingen
{
    using System;
    using Sirenix.OdinInspector;
    using UnityEngine;

    [Serializable]
    public struct StatValue<T> : IEquatable<StatValue<T>>
    {
        [HideInInspector]
        public T Type;

        [Range(0, 10)]
        [LabelWidth(70)]
        [LabelText("$Type")]
        public int Value;

        public StatValue(T type, int value)
        {
            this.Type = type;
            this.Value = value;
        }

        public StatValue(T type)
        {
            this.Type = type;
            this.Value = 0;
        }

        public bool Equals(StatValue<T> other)
        {
            return this.Type.Equals(other.Type) && this.Value == other.Value;
        }
    }
}