﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class StructureData : CardData
    {
        [BoxGroup(STATS_BOX_GROUP)]
        internal int Health;

        [VerticalGroup("Split/Right")]
        public RessourceList ressources;

        public override CardType type
        {
            get
            {
                return CardType.structure;
            }
        }
    }
}