﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class HeroData : CreatureData
    {
        public override CardType type
        {
            get
            {
                return CardType.hero;
            }
        }

        [VerticalGroup("Split/Right")]
        public ManaList ManaGenerator;
    }
}