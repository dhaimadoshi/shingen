﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class CreatureData : CardData
    {
        [BoxGroup(STATS_BOX_GROUP)]
        public int Health;
        
        [BoxGroup(STATS_BOX_GROUP)]
        public int Defense;

        [BoxGroup(STATS_BOX_GROUP)]
        public int Damage;

        public override CardType type
        {
            get
            {
                return CardType.creature;
            }
        }
    }
}