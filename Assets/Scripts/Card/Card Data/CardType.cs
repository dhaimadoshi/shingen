namespace Shingen
{
    public enum CardType
    {
        creature, spell, hero, structure
    }
}