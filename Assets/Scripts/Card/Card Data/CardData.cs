﻿namespace Shingen
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using Sirenix.Serialization;
    using UnityEngine;

    public abstract class CardData : SerializedScriptableObject
    {
        // -- Constants for Grouping in Editor --//
        protected const string LEFT_VERTICAL_GROUP
            = "Split/Left";
        protected const string STATS_BOX_GROUP
            = "Split/Left/Stats";
        protected const string GENERAL_SETTINGS_VERTICAL_GROUP
            = "Split/Left/General Settings/Split/Right";

        [HideLabel, PreviewField(55), ReadOnly]
        [VerticalGroup(LEFT_VERTICAL_GROUP)]
        [HorizontalGroup(
            LEFT_VERTICAL_GROUP + "/General Settings/Split",
            55,
            LabelWidth = 67)]
        public Sprite ManaSymbol;

        [BoxGroup(LEFT_VERTICAL_GROUP + "/General Settings")]
        [VerticalGroup(GENERAL_SETTINGS_VERTICAL_GROUP)]
        public string Name;

        [VerticalGroup(GENERAL_SETTINGS_VERTICAL_GROUP)]
        public Sprite Image;

        [VerticalGroup(GENERAL_SETTINGS_VERTICAL_GROUP)]
        [ShowInInspector]
        [InlineButton("OnChangeType")]
        // [ValidateInput("MaxCost")]
        public int ConvertedManaCost
        {
            set {}
            get
            {
                var convertedCost = 0;
                ManaCost.GetList().ForEach(x => convertedCost += x.Value);
                return convertedCost;
            }
        }

        [VerticalGroup(LEFT_VERTICAL_GROUP)]
        public ManaList ManaCost;
        
        [VerticalGroup(LEFT_VERTICAL_GROUP)]
        public RessourceList ShopCost;

        [HorizontalGroup("Split", 0.5f, MarginLeft = 5, LabelWidth = 130)]
        [VerticalGroup("Split/Right")]
        [BoxGroup("Split/Right/Effect Description")]
        [HideLabel, TextArea(4, 14)]
        public string Description;

        [OdinSerialize, NonSerialized]
        [VerticalGroup("Split/Right")]
        public List<IEffect> effect;

        public abstract CardType type { get; }

        public bool MaxCost(int cost) => cost <= 8;
        public void OnChangeType()
        {
            var manaList = ManaCost.GetList();

            if(manaList.Count < 1)
            {
                ManaSymbol = null;
                return;
            }

            ManaValue color = manaList[0];
            manaList.ForEach(x =>
            {
               if(x.Value > color.Value) color = x;
            });

            ManaSymbol = ManaHelper.GetManaSymbol(color.Type);
        }
    }
}