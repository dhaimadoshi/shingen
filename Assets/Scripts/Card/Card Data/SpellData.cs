namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SpellData : CardData
    {
        public override CardType type
        {
            get
            {
                return CardType.spell;
            }
        }
    }
}