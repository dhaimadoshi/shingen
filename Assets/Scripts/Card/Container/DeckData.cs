﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class DeckData : SerializedScriptableObject
    {
        public string Name;
        public Dictionary<CardData, int> DeckList;
    }
}