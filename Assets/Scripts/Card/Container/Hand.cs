namespace Shingen
{
    using System.Collections.Generic;
    using System.Linq;
    using ModestTree;
    using Sirenix.OdinInspector;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using Zenject;

    public class Hand : MonoBehaviour
    {
        public List<Card> hand;

        [Required, SceneObjectsOnly]
        public Transform container;

        public IBattleField battleField;

        [Inject]
        public void Construct(IBattleField battleField)
        {
            this.battleField = battleField;
        }

        public void Awake()
        {
            foreach (Card c in container.GetComponentsInChildren<Card>())
            {
                Add(c);
            }

            SetActive(false);
        }

        public List<Card> GetPlayable() => hand.Where(battleField.CanCast).ToList();

        public void SetPlayableActive() => GetPlayable().ForEach(x => x.SetActive(true));

        public void SetActive(bool v) => hand.ForEach(x => x.SetActive(v));

        public void Add(Card c)
        {
            Assert.IsNotNull(c);
            
            hand.Add(c);
            c.transform.SetParent(container);
            c.transform.localScale = Vector3.one;
            c.cardMoved += OnCardMoved;
        }

        public void OnCardMoved(PointerEventData eventData)
        {
            var card = eventData.pointerDrag.GetComponent<Card>();
            
            if (!hand.Remove(card)) Debug.LogWarning(
                    "trying to remove - "
                    + card.Data.name +
                    " - but isn't in the hand anymore");

            card.cardMoved -= OnCardMoved;
            battleField.Play(card);
        }
    }
}