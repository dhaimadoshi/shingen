﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    [System.Serializable]
    public class Deck
    {
        readonly Card.Factory _cardFactory;
        private List<CardData> _workingDeck;
        private List<CardData> _heroes;
        private List<CardData> _structure;

        public Deck(Card.Factory cardFactory)
        {
            _cardFactory = cardFactory;
        }
        public void SetDeck(DeckData data)
        {
            _workingDeck = new List<CardData>();
            _heroes = new List<CardData>();
            _structure = new List<CardData>();

            var cards = data.DeckList.Keys;

            foreach (CardData c in cards)
            {
                int count;
                data.DeckList.TryGetValue(c, out count);

                for (int i = 0; i < count; i++)
                {
                    Add(c);
                }
            }
        }

        internal Card DrawIndex(int value)
        {
            switch (value)
            {
                case 0: return Draw();
                case 1: return DrawHero();
                case 2: return DrawStructure();
            }

            return null;
        }

        private Card DrawHelper(List<CardData> list)
        {
            if (list.Count < 1)
            {
                Debug.LogWarning("trying to draw from empty deck");
                return null;
            }

            var rnd = Random.Range(0, list.Count);

            var card = list[rnd];
            list.Remove(card);

            return _cardFactory.Create(card);

        }

        public Card Draw()
        {
            return DrawHelper(_workingDeck);
        }

        public Card DrawHero()
        {
            return DrawHelper(_heroes);
        }

        public Card DrawStructure()
        {
            return DrawHelper(_structure);
        }

        public void Add(CardData c)
        {
            switch (c.type)
            {
                case CardType.hero:
                    _heroes.Add(c);
                    break;
                case CardType.structure:
                    _structure.Add(c);
                    break;
                default:
                    _workingDeck.Add(c);
                    break;
            }
        }
    }
}