﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class DirectDamage : IEffect
    {
        public int damage;
        public string description => "Deals " + damage + " damage to a target.";
    }
}