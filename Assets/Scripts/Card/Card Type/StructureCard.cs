﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using ModestTree;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class StructureCard : ICardType
    {
        public RessourceList ressources;
        public StructureCard(StructureData data) 
        {
            ressources = data.ressources;
        }

        public bool isDead
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        public void Play(IBattleField battleField)
        {
        }

        public void Target(ICardType card, CardType type)
        {
            throw new System.NotImplementedException();
        }
    }
}