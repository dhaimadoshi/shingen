﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class SpellCard : ICardType
    {
        readonly SpellData _data;
        public SpellCard(SpellData data) 
        {
            _data = data;
        }

        public bool isDead
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        public void Play(IBattleField battleField)
        {
        }

        public void Target(ICardType card, CardType type)
        {
            
        }
    }
}