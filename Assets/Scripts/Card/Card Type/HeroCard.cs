﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class HeroCard : CreatureCard
    {
        public ManaList manaSource;

        public HeroCard(HeroData data) : base(data)
        {
            manaSource = data.ManaGenerator;
        }

        public override void Play(IBattleField battleField)
        {
        }
    }
}