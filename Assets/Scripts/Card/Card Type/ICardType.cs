namespace Shingen
{
    public interface ICardType
    {
        bool isDead { get; }

        void Target(ICardType card, CardType type);
        void Play(IBattleField battleField);
    }
}