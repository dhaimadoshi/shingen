﻿namespace Shingen
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using ModestTree;
    using Sirenix.OdinInspector;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public delegate void TakingDamage();
    public class CreatureCard : ICardType
    {
        public int power;
        public int health;
        public int defense;

        public TakingDamage damageTaken;

        public bool isDead
        {
            get
            {
                return health <= 0;
            }
        }

        public CreatureCard(CreatureData data)
        {
            power = data.Damage;
            health = data.Health;
            defense = data.Defense;
        }

        public void TakeDamage(int damage)
        {
            health -= Mathf.Max(damage - defense, 0);
            damageTaken?.Invoke();
        }

        public void Heal(int heal) => health += heal;

        public void Target(ICardType card, CardType type)
        {
            switch (type)
            {
                case CardType.creature:
                    (card as CreatureCard).TakeDamage(power);
                    break;

                case CardType.structure:
                    throw new NotImplementedException();
                
                case CardType.hero:
                    throw new NotImplementedException();

                default:
                    Debug.LogError("Not supported type");
                    break;
            }
        }

        public virtual void Play(IBattleField battleField)
        {
            Debug.Log("playing Monster");
        }
    }
}