namespace Shingen
{
    using System;
    using Sirenix.OdinInspector;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    using Zenject;

    public delegate void CardPlayed(PointerEventData eventData);

    [RequireComponent(typeof(Draggable))]
    public class Card : MonoBehaviour,
    IBeginDragHandler,
    IEndDragHandler
    {
        // public CardData data { get; private set; }
        public CardData Data { get; private set; }

        [Required, SceneObjectsOnly]
        public Image Collider;

        [Required, SceneObjectsOnly]
        public Image Highlight;

        public event CardPlayed cardMoved;
        public new string name { get { return Data?.name; } }
        public int cost;

        private GameController gameController;
        private DragData dragData;
        public ICardType cardBody;

        [Inject]
        public void Construct(
            CardData data,
            DragData dragData)
        {
            Data = data;
            this.dragData = dragData;

            if (data != null)
            {
                cost = data.ConvertedManaCost;
                switch (data.type)
                {
                    case CardType.creature:
                        cardBody = new CreatureCard(data as CreatureData);
                        break;

                    case CardType.hero:
                        cardBody = new HeroCard(data as HeroData);
                        break;

                    case CardType.spell:
                        cardBody = new SpellCard(data as SpellData);
                        break;

                    case CardType.structure:
                        cardBody = new StructureCard(data as StructureData);
                        break;

                    default:
                        Debug.LogError("Type not supported");
                        break;
                }
            }

            SetActive(false);
        }

        internal void Die()
        {
            GameObject.Destroy(gameObject);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            dragData.draggedCard = this;
            GameController.ChangeState(GameState.prepareCast);
            SetHighlight(false);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData.used)
            {
                cardMoved?.Invoke(eventData);
            }

            GameController.ChangeState(GameState.wait);
        }

        public void SetActive(bool value)
        {
            Collider.gameObject.SetActive(value);
            SetHighlight(value);
        }

        public void SetHighlight(bool value)
        {
            Highlight.gameObject.SetActive(value);
        }

        internal void Target(Card target)
        {
            if (target == null)
            {
                Debug.Log(gameObject.name + " has not target");
                return;
            }

            cardBody.Target(target.cardBody, target.Data.type);
        }

        public virtual void Play(IBattleField battleField)
        {
            cardBody.Play(battleField);
        }

        public class Factory : PlaceholderFactory<CardData, Card> {}
    }
}