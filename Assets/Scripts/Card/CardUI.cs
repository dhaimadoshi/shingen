﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class CardUI : MonoBehaviour
    {
        public TextMeshProUGUI
            nameText,
            atkText,
            defenseText,
            costText,
            healthText,
            description;

        public Image picture, manaSymbol;
        public GameObject manaSymbols, manaSource, ressourceSymbols;

        public GameObject shieldIcon, atkIcon;

        private Card card;

        public void Awake()
        {
            card = GetComponent<Card>();

            SetText();
            nameText.SetText(card.name);
            picture.sprite = card.Data.Image;
        }

        public void SetText()
        {
            costText.SetText(card.cost.ToString());

            List<ManaType> manaList = new List<ManaType>();

            card.Data.ManaCost.GetList().ForEach(x =>
            {
                for (int i = 0; i < x.Value; i++)
                {
                    manaList.Add(x.Type);
                }
            });

            var manaObjects = manaSymbols.GetComponentsInChildren<Image>();

            for (int i = 0; i < manaObjects.Length; i++)
            {
                if (manaList.Count <= i)
                {
                    manaObjects[i].gameObject.SetActive(false);
                }
                else
                {
                    manaObjects[i].gameObject.SetActive(true);
                    manaObjects[i].sprite = ManaHelper.GetManaSymbol(manaList[i]);
                }
            }

            // manaSymbol.sprite = card.Data.ManaSymbol;

            description.SetText(card.Data.Description);

            if (card.Data.type == CardType.creature)
            {
                var body = card.cardBody as CreatureCard;
                healthText.SetText(body.health.ToString());
                atkText.SetText(body.power.ToString());
                defenseText.SetText(body.defense.ToString());
                body.damageTaken += SetText;
            }


            List<ManaType> manaSourceList = new List<ManaType>();
            if (card.Data.type == CardType.hero)
            {
                (card.Data as HeroData).ManaGenerator.GetList().ForEach(x =>
                {
                    for (int i = 0; i < x.Value; i++)
                    {
                        manaSourceList.Add(x.Type);
                    }
                });
            }

            var manaSourceObjects = manaSource.GetComponentsInChildren<Image>();

            for (int i = 0; i < manaSourceObjects.Length; i++)
            {
                if (manaSourceList.Count <= i)
                {
                    manaSourceObjects[i].gameObject.SetActive(false);
                }
                else
                {
                    manaSourceObjects[i].gameObject.SetActive(true);
                    manaSourceObjects[i].sprite = ManaHelper.GetManaSymbol(manaSourceList[i]);
                }
            }


            List<RessourceType> ressourceSymbolsList = new List<RessourceType>();
            if (card.Data.type == CardType.structure)
            {
                shieldIcon.gameObject.SetActive(false);
                atkIcon.gameObject.SetActive(false);

                (card.Data as StructureData).ressources.GetList().ForEach(x =>
                {
                    for (int i = 0; i < x.Value; i++)
                    {
                        ressourceSymbolsList.Add(x.Type);
                    }
                });
            }

            var ressourceSymbolsObjects = ressourceSymbols.GetComponentsInChildren<Image>();

            for (int i = 0; i < ressourceSymbolsObjects.Length; i++)
            {
                if (ressourceSymbolsList.Count <= i)
                {
                    ressourceSymbolsObjects[i].gameObject.SetActive(false);
                }
                else
                {
                    ressourceSymbolsObjects[i].gameObject.SetActive(true);
                    ressourceSymbolsObjects[i].sprite = RessourcesHelper.GetRessourceSymbol(ressourceSymbolsList[i]);
                }
            }
        }
    }
}
