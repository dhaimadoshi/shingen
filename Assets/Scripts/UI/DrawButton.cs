namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using Sirenix.OdinInspector;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;
    using Zenject;

    [RequireComponent(typeof(Button))]
    public class DrawButton : MonoBehaviour
    {
        private Hand hand;
        private Player player;
        private Button button;
        private TextMeshProUGUI text;
        private TMP_Dropdown dropdown;

        [Inject]
        public void Construct(Hand hand, Player player)
        {
            this.hand = hand;
            this.player = player;
        }

        public void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(Draw);
            text = GetComponentInChildren<TextMeshProUGUI>();
            dropdown = GetComponentInChildren<TMP_Dropdown>();
            text?.SetText("Draw");
        }

        public void Draw()
        {
            var drawnedCard = player.DrawIndex(dropdown.value);
            
            if(drawnedCard != null) 
                hand.Add(drawnedCard);

            GameController.ChangeState(GameState.wait);
        }
    }
}