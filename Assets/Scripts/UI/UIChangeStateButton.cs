﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    [RequireComponent(typeof(Button))]
    public class UIChangeStateButton : MonoBehaviour
    {
        public GameState state;
        private Button button;
        private TextMeshProUGUI text;

        public void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(ChangeState);
            text = GetComponentInChildren<TextMeshProUGUI>();
            text?.SetText(state.ToString());
        }

        public void ChangeState()
        {
            GameController.ChangeState(state);
        }

    }
}