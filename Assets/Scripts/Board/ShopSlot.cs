﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using ModestTree;
    using TMPro;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    using Zenject;

    public class ShopSlot : MonoBehaviour, IPointerClickHandler
    {
        private Card _card;
        private Player _player;
        public TextMeshProUGUI costText;

        public bool isEmpty => _card == null;

        [Inject]
        public void Construct(Player player)
        {
            _player = player;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_player.HasRessources(_card.Data.ShopCost))
            {
                Debug.LogWarning("Doesn't have ressources");
                return;
            }
            _player.DepleteRessources(_card.Data.ShopCost);

            Select();
        }

        public void Select()
        {
            _player.AddCardToDeck(_card.Data);
            ReleaseCard();
        }

        public void SetActive(bool b)
        {
            if (_card == null) return;

            _card.SetActive(b);
            // collider.gameObject.SetActive(b);
        }

        public void Add(Card card)
        {
            Assert.IsNull(this._card, "already assigned");

            string cost = "";

            card.Data.ShopCost.GetList().ForEach(x =>
            {
                cost += x.Type;
                cost += " " + x.Value + " -- ";
            });
            
            costText.SetText(cost);

            this._card = card;
            _card.transform.SetParent(transform);
            _card.transform.localPosition = Vector3.zero;
            SetActive(true);
        }

        public CardData ReleaseCard()
        {
            if (_card == null) return null;

            var data = _card.Data;

            SetActive(false);
            GameObject.Destroy(_card.gameObject);
            _card = null;

            costText.SetText("");

            return data;
        }
    }
}