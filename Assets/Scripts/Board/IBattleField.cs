namespace Shingen
{
    using System.Collections.Generic;

    public interface IBattleField
    {
        bool HasTargets(Card x);
        List<CardSlot> GetMovableHeroes();
        List<CardSlot> GetSlots();
        List<CardSlot> GetTargets(Card draggedCard);
        List<CardSlot> GetCreatureSlots();
        List<CardSlot> GetStructureSlots();
        List<CardSlot> GetHeroSlots();
        void Play(Card card);
        bool CanCast(Card arg);
        ITower tower { get; }
    }
}