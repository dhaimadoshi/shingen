﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Sirenix.Utilities;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class Shop : IInitializable
    {
        readonly Deck _deck;
        readonly DeckData _shopData;
        readonly List<ShopSlot> _shopSlots;
        readonly GameObject _gameObject;

        public Shop(Deck deck, DeckData shopData, List<ShopSlot> shopSlots, GameObject gameObject)
        {
            _deck = deck;
            _shopData = shopData;
            _shopSlots = shopSlots;
            _gameObject = gameObject;
        }

        public void Initialize()
        {
            _deck.SetDeck(_shopData);
            Close();
        }

        public void Close()
        {
            _shopSlots.Where(x => !x.isEmpty).ForEach(x => _deck.Add(x.ReleaseCard()));
            _gameObject.SetActive(false);
        }

        public void Open()
        {
            _gameObject.SetActive(true);
            _shopSlots.ForEach(x => x.Add(_deck.Draw()));
        }
    }
}