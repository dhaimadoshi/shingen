﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using ModestTree;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class CardSlotRow : MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        private List<CardSlot> slots;

        [Required]
        public Transform container;

        public bool defaultActivation;
        public bool isPlayerOwner;

        public CardType acceptedType;

        [AssetsOnly]
        public CardSlot slotPrefab;
        public int Count => slots.Count;
        public int defaultSlotNumber = 5;

        public void Awake()
        {
            if(slots == null || slots.IsEmpty()) SetSlots(defaultSlotNumber);
        }

        public CardSlot this[int index] => slots[index];
        
        private void Add(CardSlot c)
        {
            c.index = slots.Count;
            slots.Add(c);
            c.SetActive(defaultActivation);
            c.SetOwner(isPlayerOwner);
            c.acceptedType = acceptedType;
        }

        public void SetSlots(int count)
        {
            slots = new List<CardSlot>();

            foreach (CardSlot c in container.GetComponentsInChildren<CardSlot>())
            {
                Add(c);
            }

            Assert.That(slots.Count <= count, "The number of preset slots exceed the requested count of :" + count);

            for (int i = slots.Count; i < count; i++)
            {
                Add(GameObject.Instantiate(slotPrefab, Vector3.zero, Quaternion.identity, container));
            }
        }

        public List<CardSlot> GetSlots() => slots.ToList();

        public List<CardSlot> GetEmpty(bool empty)
        {
            return slots.Where(x => x.isEmpty == empty).ToList();
        }

        public void SetAllActive(bool value)
        {
            foreach (CardSlot c in slots)
            {
                c.SetActive(value);
            }
        }
    }
}