namespace Shingen
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine.UI;
    using Zenject;

    public class SimpleRessourcesCache
    {
        public RessourceList Ressources { get; private set; }
        private List<Image> _ressourceSymbols;
        public int maxRessource = 24;

        public void Initialize(List<Image> ressourceSymbols, RessourceList ressources)
        {
            _ressourceSymbols = ressourceSymbols;
            Ressources = ressources;
            SetUI();
        }

        public void AddRessources(RessourceValue r)
        {
            var convertedCost = Ressources.GetList().Select(x => x.Value).Sum();
            if(convertedCost + r.Value > maxRessource) return;
            
            Ressources[r.Type] += r.Value;
            SetUI();
        }

        public void DepleteRessources(RessourceValue r)
        {
            Ressources[r.Type] -= r.Value;
            SetUI();
        }

        public bool HasRessources(RessourceValue r)
        {
            return Ressources[r.Type] >= r.Value;
        }
        
        private void SetUI()
        {
            int index = 0;

            Ressources.GetList().ForEach(x => {
                for(int i = 0; i < x.Value; i++)
                {
                    _ressourceSymbols[index].sprite = RessourcesHelper.GetRessourceSymbol(x.Type);
                    _ressourceSymbols[index].gameObject.SetActive(true);
                    index ++;
                }
            });

            for(int i = index; i < _ressourceSymbols.Count; i++)
            {
                _ressourceSymbols[i].gameObject.SetActive(false);
            }
        }
    }
}