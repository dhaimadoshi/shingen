namespace Shingen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ModestTree;
    using Sirenix.Utilities;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class SimpleBattleField : MonoBehaviour, IBattleField
    {
        private CardSlotRow playerRow, enemyRow;
        private SimpleRessourcesCache _battleFieldRessources;
        public GameObject ressourceSymbolsParent;
        public CardSlotRow heroSlots, structureSlots;
        public Transform content;
        public ITower tower { get; private set; }
        public Player player;

        public int size;

       [Inject] 
        public void Construct(ITower tower, SimpleRessourcesCache ressources, RessourceList battleFieldRessources, Player player)
        {
            this.tower = tower;
            _battleFieldRessources = ressources;
            var ressourceSymbols = ressourceSymbolsParent.GetComponentsInChildren<Image>().ToList();
            _battleFieldRessources.Initialize(ressourceSymbols, battleFieldRessources);
            this.player = player;
        }

        public void Awake()
        {
            Assert.That(content.GetComponentsInChildren<CardSlotRow>().Length == 2,
                "BattleField needs 2 cardSlotRow but has : " + content.childCount);

            var rows = content.GetComponentsInChildren<CardSlotRow>();
            rows.ForEach(x => x.SetSlots(size));

            enemyRow = rows[0];
            enemyRow.isPlayerOwner = false;
            enemyRow.acceptedType = CardType.creature;
            playerRow = rows[1];
            playerRow.isPlayerOwner = true;
            playerRow.acceptedType = CardType.creature;
        }

        private void Fight()
        {
            for(int i = 0; i < playerRow.Count; i ++) 
            {
                playerRow[i].card?.Target(enemyRow[i].card);
                enemyRow[i].card?.Target(playerRow[i].card);
            }

            CleanBoard();
        }

        private void CleanBoard()
        {
            GetCreatureSlots().Where(x => !x.isEmpty && x.card.cardBody.isDead)
                .ForEach(x => 
                {
                    x.card.Die();
                    x.SetCard(null);
                });

            // if(GetSlots().Where(x => !x.isPlayerOwner && !x.isEmpty).IsEmpty())
            // {
            //     GameController.ChangeState(GameState.preparingHeroesMove);
            // }
            // else
            // {
            //     GameController.ChangeState(GameState.wait);
            // }
        }

        public List<CardSlot> GetCreatureSlots() => GetSlots().Where(x => x.AcceptsType(CardType.creature)).ToList();

        public List<CardSlot> GetHeroSlots() => GetSlots().Where(x => x.AcceptsType(CardType.hero)).ToList();

        public List<CardSlot> GetMovableHeroes()
        {
            return GetHeroSlots().Where(x => !x.isEmpty).ToList();
        }

        public List<CardSlot> GetSlots()
        {
            return playerRow.GetSlots()
                .Union(enemyRow.GetSlots())
                .Union(heroSlots.GetSlots())
                .Union(structureSlots.GetSlots())
                .ToList();
        }

        public List<CardSlot> GetPlayerSlots() => playerRow.GetSlots();
        public List<CardSlot> GetEnemySlots() => enemyRow.GetSlots();

        public List<CardSlot> GetStructureSlots() => 
            GetSlots().Where(x => x.AcceptsType(CardType.structure)).ToList();

        public List<CardSlot> GetTargets(Card draggedCard)
        {
            switch (draggedCard.Data.type)
            {
                case CardType.creature:
                    return GetCreatureSlots()
                        .Where(x => x.isEmpty && x.isPlayerOwner)
                        .ToList();

                case CardType.hero:
                    return GetHeroSlots()
                        .Where(x => x.isEmpty && x.isPlayerOwner)
                        .ToList();                    

                case CardType.spell:
                    return GetCreatureSlots()
                        .Where(x => !x.isEmpty)
                        .ToList();                    

                case CardType.structure:
                    return GetStructureSlots()
                        .Where(x => x.isEmpty && x.isPlayerOwner)
                        .ToList();                    

                default:
                    throw new Exception("type : " + draggedCard.Data.type + " isn't supported");
            }
        }

        public bool HasTargets(Card x)
        {
            return !GetTargets(x).IsNullOrEmpty();
        }

        public void Play(Card card)
        {
            card.Play(this);
            tower.SpendMana(card.Data.ManaCost);
        }

        public bool CanCast(Card card)
        {
            return tower.CanCast(card.Data.ManaCost);
        }

        public void NextTurn()
        {
            Fight();

            GetHeroSlots().Where(x => !x.isEmpty)
                .Select(x => x.card.cardBody as HeroCard)
                .ForEach(x => tower.IncreaseMana(x.manaSource));

            var ressources = new RessourceList();
            
            GetStructureSlots().Where(x => !x.isEmpty)
                .Select(x => x.card.cardBody as StructureCard)
                .SelectMany(x => x.ressources.GetList())
                .ForEach(x => ressources[x.Type] += x.Value);

            player.AddRessourcesWithConstraint(ressources.GetList(), _battleFieldRessources.Ressources.GetList());

            GameController.ChangeState(GameState.wait);
        }
    }
}