namespace Shingen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class SimpleTower : ITower
    {
        public ManaList mana { get; private set; }
        public int maxMana = 16;

        public event SpendingMana manaUpdate;
        private List<Image> manaSymbols;

        public SimpleTower(List<Image> manaSymbols)
        {
            this.manaSymbols = manaSymbols;

            mana = new ManaList();

            foreach(Image i in manaSymbols) i.gameObject.SetActive(false);
            SetUI();
        }

        public void IncreaseMana(ManaValue value) => IncreaseMana(value.Type, value.Value);
        public void IncreaseMana(ManaType type, int amount)
        {
            var convertedMana = mana.GetList().Select(x => x.Value).Sum();
            if(convertedMana + amount > maxMana) return;

            mana[type] += amount;
            SetUI();
            // var value = mana + amount;
            // mana = Mathf.Clamp(value, 0, maxMana);
            // manaUpdate?.Invoke();
        }

        public void IncreaseMana(ManaList source)
        {
            source.GetList().ForEach(x => IncreaseMana(x.Type, x.Value));
        }

        public void DecreaseManaSource(ManaList source)
        {
            source.GetList().ForEach(x => IncreaseMana(x.Type, -x.Value));
        }

        private void SetUI()
        {
            int index = 0;

            mana.GetList().ForEach(x => {
                for(int i = 0; i < x.Value; i++)
                {
                    manaSymbols[index].sprite = ManaHelper.GetManaSymbol(x.Type);
                    manaSymbols[index].gameObject.SetActive(true);
                    index ++;
                }
            });

            for(int i = index; i < manaSymbols.Count; i++)
            {
                manaSymbols[i].gameObject.SetActive(false);
            }
        }

        public void SpendMana(ManaList manaCost)
        {
            manaCost.GetList().ForEach(x => mana[x.Type] -= x.Value);
            SetUI();
        }

        public bool CanCast(ManaList manaCost)
        {
            var missingMana = manaCost.GetList().Where(x => mana[x.Type] < x.Value).ToList();
            return missingMana.Count <= 0;
        }
    }
}