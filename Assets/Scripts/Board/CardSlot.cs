﻿namespace Shingen
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using ModestTree;
    using Sirenix.OdinInspector;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class CardSlot : MonoBehaviour, IDropHandler
    {
        [Required, SceneObjectsOnly]
        public Image imageCollider;

        [Required, SceneObjectsOnly]
		public Transform container;		

		[ReadOnly, ShowInInspector]
		public Card card { get; private set; }

		[Required]
        public CardType acceptedType;

        public bool defaultActivation;
        internal bool isEmpty => card == null;
		public bool isPlayerOwner { get; private set; }
		public int index;

        internal bool AcceptsType(CardType type) => acceptedType == type;
        public void Awake()
		{
			SetActive(defaultActivation);

			if(container.childCount < 1) return;

			Assert.That(container.childCount <= 1, "Slot is expected to have 1 children but had : " + container.childCount);

			SetCard(container.GetComponentInChildren<Card>());
		}

        internal void SetOwner(bool v) => this.isPlayerOwner = v;

        public void SetCard(Card c)
		{
			if(c == null)
			{
				if(card != null) card.cardMoved -= OnCardMoved;
				card = null;
				return;
			}

			card = c;
			SetActive(false);
			card.cardMoved += OnCardMoved;
		}

        public void OnCardMoved(PointerEventData eventData)
		{
			if(eventData.selectedObject == container.gameObject) return;

			var droppedCard = eventData.pointerDrag.GetComponent<Card>();

			Assert.IsEqual(card, droppedCard, 
				"Trying to remove card " + droppedCard + " from slot " +
				"but card " + card + " is in the slot");

			droppedCard.cardMoved -= OnCardMoved;
			SetCard(null);
		}

		public void SetActive(bool active)
		{
			imageCollider.gameObject.SetActive(active);
			card?.SetActive(active);
		}

        public void OnDrop(PointerEventData eventData)
        {
			var droppedCard = eventData.pointerDrag.GetComponent<Card>();

			Assert.IsNotNull(droppedCard, "dragged item isn't a card");

			if(isEmpty)
			{
				Assert.That(acceptedType == droppedCard.Data.type,
					"CardSlot " + gameObject + "accepts type " + acceptedType +" but was " + droppedCard.Data.type);

				SetCard(droppedCard);
            	
				eventData.Use();
				eventData.selectedObject = container.gameObject;
			}
			else
			{
				droppedCard.Target(card);
			}

        }
    }
}