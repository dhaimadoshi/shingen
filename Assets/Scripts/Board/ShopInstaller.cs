namespace Shingen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Sirenix.OdinInspector;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class ShopInstaller : MonoInstaller<ShopInstaller>
    {
        public DeckData shopData;
        public GameObject shopSlotsParent;
        public GameObject gameObject;
        public Button closeButton;
        public Button openButton;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<Shop>().AsSingle().WithArguments(
                shopData, 
                gameObject, 
                shopSlotsParent.GetComponentsInChildren<ShopSlot>().ToList());

            closeButton.onClick.AddListener(CloseShop);
            openButton.onClick.AddListener(OpenShop);
        }

        public void CloseShop()
        {
            GameController.ChangeState(GameState.wait);
        }

        public void OpenShop()
        {
            GameController.ChangeState(GameState.shopping);
        }
    }
}