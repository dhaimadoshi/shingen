namespace Shingen
{
    public delegate void SpendingMana();
    public interface ITower
    {
        event SpendingMana manaUpdate;
        ManaList mana { get; }
        void IncreaseMana(ManaType type, int amount);
        void IncreaseMana(ManaList source);
        void SpendMana(ManaList manaCost);
        bool CanCast(ManaList manaCost);
    }
}