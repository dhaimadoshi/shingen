namespace Shingen
{
    using TMPro;

    public class SimpleBoardUI
    {
        public TextMeshProUGUI mana;
        public ITower tower;

        public SimpleBoardUI(TextMeshProUGUI manaText)
        {
            this.mana = manaText;
        }
        
        public void SetTower(ITower tower)
        {
            this.tower = tower;
            tower.manaUpdate += OnManaSpent;

            SetText();
        }

        public void SetText()
        {
            mana.SetText(tower.mana.ToString());
        }

        public void OnManaSpent()
        {
            SetText();
        }
    }
}