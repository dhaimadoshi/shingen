namespace Shingen
{
    using System;
    using Sirenix.OdinInspector;
    using UnityEngine;

    [Serializable]
    public struct RessourceValue : IEquatable<RessourceValue>
    {
        [HideInInspector]
        public RessourceType Type;

        [Range(0, 10)]
        [LabelWidth(70)]
        [LabelText("$Type")]
        public int Value;

        public RessourceValue(RessourceType type, int value)
        {
            this.Type = type;
            this.Value = value;
        }

        public RessourceValue(RessourceType type)
        {
            this.Type = type;
            this.Value = 0;
        }

        public bool Equals(RessourceValue other)
        {
            return this.Type == other.Type && this.Value == other.Value;
        }
    }
}