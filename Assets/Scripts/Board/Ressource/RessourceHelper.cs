namespace Shingen
{
    using System;
    using UnityEngine;

    public static class RessourcesHelper
    {
        public static readonly string wood = "Textures/wood";
        public static readonly string stone = "Textures/stone";

        public static Sprite GetRessourceSymbol(RessourceType ressourceType)
            => Resources.Load<Sprite>(GetPath(ressourceType));
        public static string GetPath(RessourceType ressourceType)
        {
            switch (ressourceType)
            {
                case RessourceType.wood:
                    return wood;

                case RessourceType.stone:
                    return stone;

                default:
                    throw new Exception("card color not handled");
            }
        }
    }
}