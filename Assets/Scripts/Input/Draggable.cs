﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using DG.Tweening;
    using Sirenix.OdinInspector;
    using UnityEngine;
    using UnityEngine.EventSystems;

    /*
        Precondition:
            The anchor settings for the movable graphic and the root 
            transform on which this script is attached needs to be 
            exactly the same.
            The size of both RectTransform should be the same as well
     */
    public class Draggable : 
    MonoBehaviour,
    IBeginDragHandler,
    IDragHandler,
    IEndDragHandler
    {
        // -- Public variable && Settings --------------------------- //

        [TitleGroup("Draggable Settings")]
        public RectTransform movable;
        [TitleGroup("Draggable Settings")]
        public bool parentOnDrop = true;


        public RectTransform parentRect =>
            movable.parent.GetComponent<RectTransform>();

        [TitleGroup("Drag move Settings")]
        public float dragDelay;

        [TitleGroup("Drag move Settings")]
        public Ease dragEase;

        [TitleGroup("Bounce Settings")]
        public bool bounceBack = false;

        [TitleGroup("Bounce Settings")]
        [ShowIf("bounceBack")]
        public float bounceDelay;

        [TitleGroup("Bounce Settings")]
        [ShowIf("bounceBack")]
        public Ease bounceEase;

        public event PointerEvent beginDrag, endDrag;

        // -- Private variables  ------------------------------------- //
        private Vector2 _offset;
        private Vector2 _initialPosition;
        private Vector3 _initialSize;

        // -- Functions : Unity Messages ----------------------------- // 
        public void Awake()
        {
            if (movable == null)
                movable = GetComponent<RectTransform>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            beginDrag?.Invoke(eventData);

            // if drag begin before bounce back finishes : restore the initial position correctly
            // to avoid initial position to be changed when picked on the fly
            movable.DOKill();
            movable.anchoredPosition = _initialPosition;

            _initialPosition = movable.anchoredPosition;

            Canvas[] c = GetComponentsInParent<Canvas>();
            Canvas topmost = c[c.Length - 1];

            _initialSize = topmost.GetComponent<RectTransform>().localScale;

            // var tweener = movable.DOScale(Vector3.one / _initialSize.x, 0.25f);
            var tweener = movable.DOScale(_initialSize / movable.lossyScale.x, 0.75f);
            tweener.SetEase(Ease.OutElastic);

            // movable.localScale = Vector3.one / _initialSize.x;
            // movable.localScale = _initialSize / movable.lossyScale.x;

            Vector2 screenToLocalPos;

            RectTransformUtility
            .ScreenPointToLocalPointInRectangle(
                parentRect,
                eventData.position,
                Camera.main,
                out screenToLocalPos);

            _offset = movable.anchoredPosition - screenToLocalPos;
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector2 screenToLocalPos;

            RectTransformUtility
            .ScreenPointToLocalPointInRectangle(
                parentRect,
                eventData.position,
                Camera.main,
                out screenToLocalPos);

            var screenPos = screenToLocalPos + _offset;
            MoveToScreenPoint(screenPos, dragDelay, dragEase);
        }


        /*
            eventData.used determine if a drop occured
            eventData.selectedObject contains the object to parent to
         */
        public void OnEndDrag(PointerEventData eventData)
        {
            endDrag?.Invoke(eventData);

            movable.DOKill();

            Tweener tweener;

            if (eventData.used)
            {
                var dropzone = eventData.selectedObject;

                if (dropzone != null && parentOnDrop)
                    RelocateRoot(dropzone.transform);
            }

            tweener = movable.DOScale(Vector3.one, 0.75f);
            tweener.SetEase(Ease.OutElastic);

            if(eventData.used) return;

            if (bounceBack)
            {
                MoveToScreenPoint(_initialPosition, bounceDelay, bounceEase);
            }
            else
            {
                RelocateRoot(); // update root position so that it matches the new image position
            }
        }

        // - Public functions --------------------------------------- //
        public void MoveToScreenPoint(Vector2 screenPos, float delay, Ease ease)
        {
            // movable.DOKill();

            var tweener = movable.DOAnchorPos(screenPos, delay);
            tweener.SetEase(ease);
        }

        public void RelocateRoot(Transform newParent = null)
        {
            movable.DOKill();   // stop movement
            movable.SetParent(transform.parent);    // unparent movable to avoid jumpy anim when moving root

            Vector2 position;
            if (newParent != null)
            {
                position = Vector2.zero;
                transform.SetParent(newParent);
            }
            else
            {
                position = movable.anchoredPosition;
            }

            var rectTransform = GetComponent<RectTransform>();
            rectTransform.anchoredPosition = position;
            rectTransform.localScale = Vector3.one;

            movable.SetParent(transform);   // reparenting movable inside root
            MoveToScreenPoint(Vector2.zero, dragDelay, dragEase);
        }
    }
}