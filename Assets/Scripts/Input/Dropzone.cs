﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public delegate void PointerEvent(PointerEventData eventData);
    public class Dropzone : MonoBehaviour, IDropHandler
    {
        public event PointerEvent dropped;

        public void OnDrop(PointerEventData eventData)
        {
            eventData.Use();
			eventData.selectedObject = gameObject;

            dropped?.Invoke(eventData);
        }
    }
}