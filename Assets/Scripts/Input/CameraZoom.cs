﻿namespace Shingen
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class CameraZoom : MonoBehaviour
    {
        private Camera _cam;
        private int _current = 0;
        public int current
        {
            get
            {
                return _current;
            }
            set
            {
                if ((value > -1) && (value < steps.Count))
                {
                    _current = value;
                }
            }
        }

        public int initial;

        public List<int> steps = new List<int>(new int[] { 900, 1500, 2500, 4000 });

        void Start()
        {
            _cam = GetComponent<Camera>();

            if (_cam == null) Debug.LogError("a Camera needs to me to be attached");

            current = initial;
            _cam.orthographicSize = steps[current];
        }

        void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
            {
                current++;
                _cam.orthographicSize = steps[current];
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
            {
                current--;
                _cam.orthographicSize = steps[current];
            }
        }
    }
}