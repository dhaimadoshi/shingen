namespace Shingen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Sirenix.OdinInspector;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using Zenject;

    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        public SimpleBattleField simpleBattleField;
        public GameObject battleFieldMana;

        // public TextMeshProUGUI SimpleBattlefieldManaText;
        public Hand hand;
        public DeckData playerDeck;

        public RessourceList battleFieldRessources;
        public GameObject battleFieldRessourcesIcons;
        public GameObject playerRessourcesIcons;

        public Camera camera;


        [AssetsOnly]
        public Card cardPrefab;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GameController>().AsSingle();
            Container.BindInstance(battleFieldRessources).WhenInjectedInto<SimpleBattleField>();
            Container.Bind<IBattleField>().To<SimpleBattleField>().FromInstance(simpleBattleField);
            
            Container.BindInterfacesAndSelfTo<SimpleRessourcesCache>().AsTransient();
            
            // Container.Bind<SimpleBoardUI>().AsTransient().WithArguments(SimpleBattlefieldManaText);
            Container.Bind<ITower>().To<SimpleTower>().AsTransient()
                .WithArguments(battleFieldMana.GetComponentsInChildren<Image>().ToList());

            Container.Bind<Deck>().AsTransient();
            Container.BindFactory<CardData, Card, Card.Factory>().FromComponentInNewPrefab(cardPrefab);

            Container.Bind<WaitState>().AsSingle();
            Container.Bind<PreparingCastState>().AsSingle();
            Container.Bind<PreparingHeroesMoveState>().AsSingle().WithArguments(camera);
            Container.Bind<MovingHeroesState>().AsSingle();
            Container.Bind<ShoppingState>().AsSingle();
            
            Container.Bind<Player>().AsSingle()
                .WithArguments(playerDeck, playerRessourcesIcons.GetComponentsInChildren<Image>().ToList());

            // Container.Bind<Shop>().FromResolve();

            Container.BindInstance(hand);
            Container.Bind<DragData>().AsSingle();
        }
    }
}