﻿namespace Shingen
{
    public enum GameState
    {
        wait,
        prepareCast,
        preparingHeroesMove,
        movingHeroes,
        shopping,
    }
}