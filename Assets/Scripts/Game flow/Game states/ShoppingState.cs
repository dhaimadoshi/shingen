namespace Shingen
{
    using System.Linq;
    using UnityEngine;
    public class ShoppingState : IGameState
    {
        readonly IBattleField _battleField;
        readonly Shop _shop;

        public GameState State
        {
            get
            {
                return GameState.shopping;
            }
        }

        public ShoppingState(IBattleField battleField, Shop shop)
        {
            _battleField = battleField;
            _shop = shop;
        }
        
        public void EnterState()
        {
            _shop.Open();
        }

        public void ExitState()
        {
            _shop.Close();
        }
    }
}