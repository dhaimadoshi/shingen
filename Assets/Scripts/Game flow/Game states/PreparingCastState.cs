namespace Shingen
{
    using System.Linq;
    using Sirenix.Utilities;
    using UnityEngine;
    public class PreparingCastState : IGameState
    {
        IBattleField battleField;
        private DragData dragData;

        public PreparingCastState(IBattleField battleField, DragData dragData)
        {
            this.battleField = battleField;
            this.dragData = dragData;
        }

        public GameState State => GameState.prepareCast;

        public void EnterState()
        {
            battleField.GetTargets(dragData.draggedCard).ForEach(x => x.SetActive(true));
        }

        public void ExitState()
        {
            battleField.GetSlots().ForEach(x => x.SetActive(false));
        }
    }
}