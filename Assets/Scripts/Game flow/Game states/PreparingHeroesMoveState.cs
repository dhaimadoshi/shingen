namespace Shingen
{
    using UnityEngine;
    using DG.Tweening;

    public class PreparingHeroesMoveState : IGameState
    {
        readonly IBattleField _battleField;
        readonly Camera _camera;
        private float _currentSize;
        public float duration = 0.5f;
        public float size = 3600;

        public GameState State
        {
            get
            {
                return GameState.preparingHeroesMove;
            }
        }

        public PreparingHeroesMoveState(IBattleField battleField, Camera camera)
        {
            _battleField = battleField;
            _camera = camera;
        }

        public void EnterState()
        {
            _battleField.GetMovableHeroes().ForEach(x => x.SetActive(true));
            
            _currentSize = _camera.orthographicSize;

            _camera.DOKill();
            _camera.DOOrthoSize(size, duration);
        }

        public void ExitState()
        {
            _battleField.GetSlots().ForEach(x => x.SetActive(false));

            _camera.DOKill();
            _camera.DOOrthoSize(_currentSize, duration);
        }
    }
}