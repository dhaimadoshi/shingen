namespace Shingen
{
    using UnityEngine;
    public class MovingHeroesState : IGameState
    {
        IBattleField battleField;

        public GameState State
        {
            get
            {
                return GameState.movingHeroes;
            }
        }

        public MovingHeroesState(IBattleField battleField)
        {
            this.battleField = battleField;
        }
        
        public void EnterState()
        {
            battleField.GetMovableHeroes();
        }

        public void ExitState()
        {
        }
    }
}