namespace Shingen
{
    using System.Linq;
    using UnityEngine;
    public class WaitState : IGameState
    {
        IBattleField battleField;
        Hand playerHand;

        public GameState State
        {
            get
            {
                return GameState.wait;
            }
        }

        public WaitState(Hand hand, IBattleField battleField)
        {
            this.playerHand = hand;
            this.battleField = battleField;
        }
        
        public void EnterState()
        {
            playerHand.GetPlayable()
                .Where(battleField.HasTargets)
                .ToList()
                .ForEach(x => x.SetActive(true));
        }

        public void ExitState()
        {
            playerHand.SetActive(false);
        }
    }
}