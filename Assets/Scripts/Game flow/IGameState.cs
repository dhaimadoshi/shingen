﻿namespace Shingen
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IGameState
    {
        GameState State { get; }

        void ExitState();
        void EnterState();
    }
}