﻿namespace Shingen
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    public class GameController : IInitializable
    {
        public const bool DEBUG = false;
        public static GameController Instance { get; private set; }
        List<IGameState> states { get; set; }
        public GameState CurrentState { get { return currentState.State; } }
        private GameState PreviousState { get; set; }
        private IGameState currentState;

        private GameController(
            WaitState waitState,
            PreparingCastState preparingCastState,
            PreparingHeroesMoveState preparingHeroesMoveState,
            MovingHeroesState movingHeroesState,
            ShoppingState shoppingState
        )
        {
            if(Instance == null) Instance = this;

            states = new List<IGameState>()
            {
                waitState,
                preparingCastState,
                preparingHeroesMoveState,
                movingHeroesState,
                shoppingState
            };
        }

        public void Initialize()
        {
            ChangeState(GameState.wait);
        }

        public static void ChangeState(GameState state)
        {
            // if (Instance.currentState?.State == state)
            // {
            //     return;
            // }

            if (Instance.currentState != null)
                Instance.PreviousState = Instance.CurrentState;

            if(DEBUG)
                Debug.Log($"Changing state: {state}");

            if (Instance.currentState != null)
            {
                Instance.currentState.ExitState();
                Instance.currentState = null;
            }

            Instance.currentState = Instance.states[(int)state];
            Instance.currentState.EnterState();
        }

        public void RestaurePreviousState() => ChangeState(PreviousState);
    }
}